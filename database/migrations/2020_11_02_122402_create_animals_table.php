<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('nome_propietario');
            $table->date('data_nascimento')->nullable();
            $table->float('idade')->nullable();
            $table->string('sexo')->nullable();
            $table->string('pelagem')->nullable();
            $table->string('cor')->nullable();
            $table->string('porte')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal');
    }
}
