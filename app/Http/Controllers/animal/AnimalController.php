<?php

namespace App\Http\Controllers\animal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Animal;

class AnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $animais = Animal::all();
        return view('animal.index', [
            'animais' => $animais
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $animais = Animal::all();
        return view('animal.create', [
            'animais' => $animais
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $animal = new Animal();
        $animal->nome = $request->nome;
        $animal->nome_propietario = $request->nome_propietario;
        $animal->data_nascimento = $request->data_nascimento;
        $animal->idade = (intval(date('Y')) - intval(date('Y',strtotime($request->data_nascimento))));
        $animal->sexo = $request->sexo;
        $animal->pelagem = $request->pelagem;
        $animal->cor = $request->cor;
        $animal->porte = $request->porte;
        $animal->save();


        return redirect()->route('animal.index')
                        ->with('status','Animal cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $animal = Animal::find($id);
        
        return view('animal.show', [
            'animal' => $animal,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $animal = Animal::find($id);
        
        return view('animal.edit', [
            'animal' => $animal,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $animal = Animal::find($id);
        $animal->nome = $request->nome;
        $animal->nome_propietario = $request->nome_propietario;
        $animal->data_nascimento = $request->data_nascimento;
        $animal->idade = (intval(date('Y')) - intval(date('Y',strtotime($request->data_nascimento))));
        $animal->sexo = $request->sexo;
        $animal->pelagem = $request->pelagem;
        $animal->cor = $request->cor;
        $animal->porte = $request->porte;
        $animal->update();

        return redirect()->route('animal.index')
                        ->with('status','Animal alterado com sucesso.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $animal = Animal::find($id);
        $animal->delete();
        
        return redirect()->route('animal.index')
                        ->with('status','Animal removído.');
    }
}
