@extends('layouts.app')

@section('content')

<div class="card-body bg-white">
    <div class="container">
        <h1 class="display-4"></h1>
		
		
    </div>
</div>

@if(session()->has('status'))
   <div class="alert alert-primary" role="alert">   
        <p>{{session()->get('status')}}</p>
    </div>
@endif


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table id="tabela-animais" class="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Nome Propietário</th>
                            <th>Data de nascimento</th>
                            <th>Idade (em anos)</th>
                            <th>Sexo</th>
                            <th>Pelagem</th>
                            <th>Cor</th>
                            <th>Porte</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($animais as $animal)
                        <tr>
                            <td>{{ $animal->nome }}</td>
                            <td>{{ $animal->nome_propietario }}</td>
                            <td>{{ date('d/m/Y',strtotime($animal->data_nascimento)) }}</td>
                            <td>{{ $animal->idade }}</td>
                            <td>{{ $animal->sexo }}</td>
                            <td>{{ $animal->pelagem }}</td>
                            <td>{{ $animal->cor }}</td>
                            <td>{{ $animal->porte }}</td>
                            
                            <td>
                                <a href="{{ route('animal.edit',$animal->id) }}" class="btn btn-outline-primary role="button">
                                    Editar
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                </a>
                                <form action="{{ route('animal.destroy', $animal->id) }}" method="POST">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button class="btn btn-outline-primary">
                                        Excluir
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-x-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M4 0a2 2 0 0 0-2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4zm2.854 5.146a.5.5 0 1 0-.708.708L7.293 7 6.146 8.146a.5.5 0 1 0 .708.708L8 7.707l1.146 1.147a.5.5 0 1 0 .708-.708L8.707 7l1.147-1.146a.5.5 0 0 0-.708-.708L8 6.293 6.854 5.146z"/>
                                        </svg>
                                    </button>
                                </form>

        
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a href="{{ route('animal.create') }}" title="Placeholder link title" class="text-decoration-none">
                Cadastrar animal
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bookmark-plus-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M4 0a2 2 0 0 0-2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4zm4.5 4.5a.5.5 0 0 0-1 0V6H6a.5.5 0 0 0 0 1h1.5v1.5a.5.5 0 0 0 1 0V7H10a.5.5 0 0 0 0-1H8.5V4.5z"/>
                </svg>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection


