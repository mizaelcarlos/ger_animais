@extends('layouts.app')

@section('content')
</br>        
        <div class="row">
            <div class="col-md-12">
            <div class="card">
                <div class="card-header">Alterar animal </div>
                    <div class="card-body">
                        <form action="{{ route('animal.update',$animal->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="card-body bg-white">

                                    
                            <div class="form-group">
                                        <label for="nome" class="control-label">{{ 'Nome ' }}</label>
                                        <input class="form-control" name="nome" type="text" id="nome" placeholder="Informe o nome do animal" value="{{ isset($animal->nome) ? $animal->nome : ''}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="nome_propietario" class="control-label">{{ 'Nome propietário' }}</label>
                                        <input class="form-control" name="nome_propietario" type="text" id="nome_propietario" placeholder="Informe o nome do propietário" value="{{ isset($animal->nome_propietario) ? $animal->nome_propietario : ''}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="data_nascimento" class="control-label">{{ 'Data de nascimento ' }}</label>
                                        <input class="form-control" name="data_nascimento" type="date" id="data_nascimento"  value="{{ isset($animal->data_nascimento) ? date('d/m/Y',strtotime($animal->data_nascimento))  : ''}}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="sexo" class="control-label">{{ 'Sexo' }}</label>
                                        <select name="sexo" class="form-control" id="sexo" required>
                                        @foreach (json_decode('{"M":"Masculino","F":"Feminino"}') as $optionKey => $optionValue)
                                            <option value="{{ $optionKey }}" {{ (isset($animal->sexo) && $animal->sexo == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="pelagem" class="control-label">{{ 'Pelagem' }}</label>
                                        <input class="form-control" name="pelagem" type="text" id="pelagem" placeholder="Informe pelagem" value="{{ isset($animal->pelagem) ? $animal->pelagem : ''}}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="cor" class="control-label">{{ 'Cor' }}</label>
                                        <input class="form-control" name="cor" type="text" id="cor" placeholder="Informe a cor" value="{{ isset($animal->cor) ? $animal->cor : ''}}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="porte" class="control-label">{{ 'Porte' }}</label>
                                        <input class="form-control" name="porte" type="text" id="porte" placeholder="Informe o porte" value="{{ isset($animal->porte) ? $animal->porte : ''}}" >
                                    </div>
                    
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-primary mb-3">
                                        Salvar 
                                    </button>
                                    </div>

                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
@endsection
