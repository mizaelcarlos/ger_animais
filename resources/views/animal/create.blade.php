@extends('layouts.app')

@section('content')
        </br>
        <div class="row">
            <div class="col-md-12">
            <div class="card">
                <div class="card-header">Cadastrar animal </div>
                    <div class="card-body">

                        <form action="{{ route('animal.store') }}" method="POST">
                          @csrf

                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        <label for="nome" class="control-label">{{ 'Nome ' }}</label>
                                        <input class="form-control" name="nome" type="text" id="nome" placeholder="Informe o nome do animal" value="{{ isset($animal->nome) ? $animal->nome : ''}}" required>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        <label for="nome_propietario" class="control-label">{{ 'Nome propietário' }}</label>
                                        <input class="form-control" name="nome_propietario" type="text" id="nome_propietario" placeholder="Informe o nome do propietário" value="{{ isset($animal->nome_propietario) ? $animal->nome_propietario : ''}}" required>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        <label for="data_nascimento" class="control-label">{{ 'Data de nascimento ' }}</label>
                                        <input class="form-control" name="data_nascimento" type="date" id="data_nascimento" placeholder="Informe a data de nascimento" value="{{ isset($animal->data_nascimento) ? $animal->data_nascimento : ''}}" >
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2">
                                        <label for="sexo" class="control-label">{{ 'Sexo' }}</label>
                                        <select name="sexo" class="form-control" id="sexo" required>
                                        @foreach (json_decode('{"M":"Masculino","F":"Feminino"}') as $optionKey => $optionValue)
                                            <option value="{{ $optionKey }}" {{ (isset($animal->sexo) && $animal->sexo == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        <label for="pelagem" class="control-label">{{ 'Pelagem' }}</label>
                                        <input class="form-control" name="pelagem" type="text" id="pelagem" placeholder="Informe pelagem" value="{{ isset($animal->pelagem) ? $animal->pelagem : ''}}" >
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        <label for="cor" class="control-label">{{ 'Cor' }}</label>
                                        <input class="form-control" name="cor" type="text" id="cor" placeholder="Informe a cor" value="{{ isset($animal->cor) ? $animal->cor : ''}}" >
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        <label for="porte" class="control-label">{{ 'Porte' }}</label>
                                        <input class="form-control" name="porte" type="text" id="porte" placeholder="Informe o porte" value="{{ isset($animal->porte) ? $animal->porte : ''}}" >
                                    </div>

                                   
                                    <div class="col-xs-3 col-sm-3 col-md-4">
                                    </br>
                                    <button type="submit" class="btn btn-primary">
                                        Salvar 
                                    </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
@endsection
